import numpy as np
import pygame
import random

# import gymnasium as gym
# from gymnasium import spaces
import gym as gym
from gym import spaces
from gym.spaces import multi_discrete, box, dict, multi_binary

# Define constants for the screen width and height
from speedball.pygame_config import (
    BLACK,
    PYGAME_LENGTH,
    PYGAME_WIDTH,
    divisor,
    LENGTH,
    WIDTH,
    offset,
    FIELD,
    PLAYER_MOVEMENT,
    BALL_MOVEMENT,
    PLAYER_TACKLE_SD,
    PLAYER_TACKLE_LIMIT,
)
from speedball.pygame_classes import (
    Red_team,
    Blue_team,
    Ball,
    pygame_get_sprites,
    pygame_get_sprites_v2,
    draw_text,
    draw_score,
)
import pygame
import statistics
import math
import pandas as pd


class SpeedballEnv(gym.Env):
    """
    The metadata holds environment constants. From gymnasium, we inherit the "render_modes",
    metadata which specifies which modes can be put into the render() method.
    At least human mode should be supported.
    The "name" metadata allows the environment to be pretty printed.
    """

    metadata = {"render_modes": [True], "name": "speedball_v3"}

    def __init__(
        self,
        render_mode=True,
        fps=15,
        observations="array",
        mode="full",
        policy=None,
        flags=[],
        players_per_team=5,
        number_of_agents=10,
        random_ball_velocity=False,
        ball_starts_with="",
        flat_action=False,
        old_api=False,
    ):
        self.players_per_team = players_per_team
        self.number_of_agents = number_of_agents
        self.observation_method = observations
        self.mode = mode
        self.fps = fps
        self.flags = flags
        self.pre_render_bool = False
        self.random_ball_velocity = random_ball_velocity
        self.ball_starts_with = ball_starts_with
        self.flat_action = flat_action
        self.old_api = old_api
        if policy:
            self.policy = policy
        """
        The init method takes in environment arguments and
         should define the following attributes:
        - possible_agents
        - action_spaces
        - observation_spaces
        These attributes should not be changed after initialization.
        """
        self.possible_agents = []
        for i, agent in enumerate(range(self.number_of_agents)):
            if i < self.players_per_team:
                team = 0  # Red
                j = i
            else:
                team = 1  # Blue
                j = i - self.players_per_team

            self.possible_agents.append("{}_{}".format(team, j))

        # self.agent_name_mapping = dict(
        #     zip(self.possible_agents, list(range(len(self.possible_agents))))
        # )

        # gymnasium spaces are defined and documented here: https://gymnasium.farama.org/api/spaces/
        if self.mode == "atari":
            self.action_space = dict.Dict(
                {
                    "direction": multi_discrete.MultiDiscrete(
                        [9 for _ in range(self.players_per_team)]
                    ),
                    "pass": multi_binary.MultiBinary(self.players_per_team),
                }
            )

        elif self.mode == "full":
            self.action_space = dict.Dict(
                {
                    "direction": multi_discrete.MultiDiscrete(
                        [361 for _ in range(self.players_per_team)]
                    ),
                    "pass": multi_binary.MultiBinary(self.players_per_team),
                }
            )
            if self.flat_action:
                self.action_space = multi_discrete.MultiDiscrete(
                    [361, 361, 361, 361, 361, 2, 2, 2, 2, 2]
                )
        else:
            print("No mode exists")
            raise Exception

        if self.observation_method == "human":
            raise Exception
            # observation_space = {
            #     a: spaces.Dict(
            #         {
            #             "x": spaces.Box(low=0, high=12),
            #             "y": spaces.Box(low=0, high=7),
            #             "has_ball": spaces.Discrete(2),
            #             "team": spaces.Discrete(2),  # red 0, blue 1
            #         }
            #     )
            #     for a in self.possible_agents
            # }
            # observation_space["ball"] = spaces.Dict(
            #     {
            #         "x": spaces.Box(low=0, high=12),
            #         "y": spaces.Box(low=0, high=7),
            #         "velocity_x": spaces.Box(low=-1, high=1),
            #         "velocity_y": spaces.Box(low=-1, high=1),
            #     }
            # )
            # self.observation_space = spaces.Dict(observation_space)
        elif self.observation_method == "array":
            self.observation_space = box.Box(-1, 12, shape=(44,))

        self.render_mode = render_mode

    def policy(self, observations, info):
        return self.action_space.sample()

    def pre_render(self):
        ### pygame stuff
        pygame.init()
        # pygame.mixer.init()
        self.screen = pygame.display.set_mode((PYGAME_LENGTH, PYGAME_WIDTH))
        pygame.display.set_caption("Speedball")
        self.clock = pygame.time.Clock()

        self.background = pygame.transform.scale(
            pygame.image.load(FIELD).convert(), (PYGAME_LENGTH, PYGAME_WIDTH)
        )
        self.background_rect = self.background.get_rect()
        self.pre_render_bool = True

    def render(self):
        """
        Renders the environment. In human mode, it can print to terminal, open
        up a graphical window, or open up some other display that a human can see and understand.
        """
        if self.pre_render_bool == False:
            self.pre_render()
        self.clock.tick(self.fps)
        agents = self.observations
        ball = self.observations["ball"]
        teams = [0, 1]
        # print(agents)
        all_sprites = pygame_get_sprites_v2(agents, ball, teams, self.flags)
        # Draw / render
        self.screen.fill(BLACK)
        self.screen.blit(self.background, self.background_rect)
        # draw_score(screen, score, 60, PYGAME_LENGTH / 2, 700)

        # *after* drawing everything, flip the display
        all_sprites.draw(self.screen)

        pygame.display.flip()

    def close(self):
        ### close pygame
        if self.render_mode:
            pygame.display.quit()
            pygame.quit()



    def observe(self, invert=False):
        """
        Observe should return the observation of the specified agent. This function
        should return a sane observation (though not necessarily the most up to date possible)
        at any time after reset() is called.
        """
        # first treat observations to be 2dp
        for agent in self.agents:
            self.observations[agent]["x"] = round(
                float(self.observations[agent]["x"]), 2
            )
            self.observations[agent]["y"] = round(
                float(self.observations[agent]["y"]), 2
            )

        self.observations["ball"]["x"] = round(float(self.observations["ball"]["x"]), 2)
        self.observations["ball"]["y"] = round(float(self.observations["ball"]["y"]), 2)
        self.observations["ball"]["velocity_x"] = round(
            float(self.observations["ball"]["velocity_x"]), 2
        )
        self.observations["ball"]["velocity_y"] = round(
            float(self.observations["ball"]["velocity_y"]), 2
        )

        # invert only works for array - it edits blue to look like red
        if self.observation_method == "human":
            return self.observations
        elif self.observation_method == "array":
            observations = []
            for key in self.observations.keys():
                for key1 in self.observations[key].keys():
                    if invert:
                        if key1 == "x":
                            obs = (self.observations[key][key1] - LENGTH) * -1
                            observations.append(obs)
                        elif key1 == "y":
                            obs = self.observations[key][key1]
                            observations.append(obs)
                        elif key1 == "velocity_x" or key1 == "velocity_y":
                            obs = (self.observations[key][key1]) * -1
                            observations.append(obs)
                        elif key1 == "team":
                            obs = int(not self.observations[key][key1])
                            observations.append(obs)
                        elif key1 == "has_ball":
                            observations.append(self.observations[key][key1])
                        else:
                            print(key1)
                            raise Exception
                    else:
                        observations.append(self.observations[key][key1])
            if invert:
                observations = (
                    observations[20:40] + observations[0:20] + observations[40:44]
                )
            return np.array(observations, dtype=np.float16)

    def inverter(self, obs, info):
        invert = True
        observations = []
        for key in info.keys():
            for key1 in info[key].keys():
                if invert:
                    if key1 == "x":
                        obs = (info[key][key1] - LENGTH) * -1
                        obs = round(float(obs), 2)
                        observations.append(obs)
                    elif key1 == "y":
                        obs = info[key][key1]
                        obs = round(float(obs), 2)
                        observations.append(obs)
                    elif key1 == "velocity_x" or key1 == "velocity_y":

                        obs = (info[key][key1]) * -1
                        obs = round(float(obs), 2)
                        if obs == 0:
                            observations.append(info[key][key1])
                        else:
                            observations.append(obs)
                    elif key1 == "team":
                        obs = int(not info[key][key1])
                        # obs = int(info[key][key1])
                        observations.append(obs)
                    elif key1 == "has_ball":
                        observations.append(info[key][key1])
                    else:
                        print(key1)
                        raise Exception
                else:
                    observations.append(info[key][key1])
        if invert:
            observations = (
                observations[20:40] + observations[0:20] + observations[40:44]
            )
        observations = np.array(observations, dtype=np.float16)
        obs_count = 0
        inverted_info = {}
        for agent in self.agents:
            inverted_info[agent] = {
                "x": round(float(observations[0 + obs_count]), 2),
                "y": round(float(observations[1 + obs_count]), 2),
                "has_ball": observations[2 + obs_count],
                "team": observations[3 + obs_count],
            }
            obs_count += 4
        inverted_info["ball"] = {
            "x": observations[len(observations)-4],
            "y": observations[len(observations)-3],
            "velocity_x": observations[len(observations)-2],
            "velocity_y": observations[len(observations)-1],
        }
        return np.array(observations), inverted_info

    def reset(self, seed=None, return_info=False, options=None):
        """
        Reset needs to initialize the following attributes
        - agents
        - rewards
        - _cumulative_rewards
        - terminations
        - truncations
        - infos
        - agent_selection
        And must set up the environment so that render(), step(), and observe()
        can be called without issues.
        Here it sets up the state dictionary which is used by step() and the observations dictionary which is used by step() and observe()
        """
        # We need the following line to seed self.np_random
        if True:#self.old_api:
            pass
        else:
            super().reset(seed=seed)
        self.agents = self.possible_agents[:]
        self.rewards = {agent: 0 for agent in self.agents}
        self._cumulative_rewards = {agent: 0 for agent in self.agents}
        self.terminations = {agent: False for agent in self.agents}
        self.truncations = {agent: False for agent in self.agents}
        self.infos = {agent: {} for agent in self.agents}
        self.state = {agent: None for agent in self.agents}

        # start agents in location:
        self.observations = {}
        for i, agent in enumerate(self.agents):
            if i < self.players_per_team:
                self.observations[agent] = {"x": 1, "y": i, "has_ball": 0, "team": 0}
            else:
                self.observations[agent] = {
                    "x": 11,
                    "y": i - self.players_per_team,
                    "has_ball": 0,
                    "team": 1,
                }

        # self.observations = {agent: NONE for agent in self.agents}

        if self.random_ball_velocity:
            self.observations["ball"] = {
                "x": 6,
                "y": 3.5,
                "velocity_x": random.random() / 2,
                "velocity_y": random.random() / 2,
            }
        else:
            self.observations["ball"] = {
                "x": 6,
                "y": 3.5,
                "velocity_x": 0,
                "velocity_y": 0,
            }
        if self.ball_starts_with == "red":
            self.observations["ball"] = {
                "x": 1,
                "y": 0,
                "velocity_x": 0,
                "velocity_y": 0,
            }
        if self.ball_starts_with == "blue":
            self.observations["ball"] = {
                "x": 11,
                "y": 0,
                "velocity_x": 0,
                "velocity_y": 0,
            }

        self.num_moves = 0
        """
        Our agent_selector utility allows easy cyclic stepping through the agents list.
        """
        if self.old_api:
            return self.observe()
        else:
            return self.observe(), self.observations

    def invert_actions(self, actions):
        inverted_action = []
        for ele in actions:
            tmp_action = int(abs(360 - ele))
            if tmp_action == 360:
                tmp_action = 0
            if ele == 360:
                tmp_action = 360
            inverted_action.append(tmp_action)
        return inverted_action

    def step(self, action, otherAction=None, invert_other_action=False):
        """
        step(action) takes in an action for the current agent (specified by
        agent_selection) and needs to update
        - rewards
        - _cumulative_rewards (accumulating the rewards)
        - terminations
        - truncations
        - infos
        - agent_selection (to the next agent)
        And any internal state used by observe() or render()
        """

        # the agent which stepped last had its _cumulative_rewards accounted for
        # (because it was returned by last()), so the _cumulative_rewards for this
        # agent should start again at 0

        if self.flat_action:
            tmp_action = {}
            tmp_action["direction"] = action[0:5]
            tmp_action["pass"] = action[5:10]

        if otherAction is None:  # override baseline policy
            obs = self.observe()
            obs, info = self.inverter(obs, self.observations)
            otherAction = self.policy(obs, self.observations)
            invert_other_action = True

        if self.flat_action:
            tmp_action = {}
            tmp_other_action = {}
            tmp_action["direction"] = action[0:5]
            tmp_action["pass"] = action[5:10]
            action = tmp_action
            tmp_other_action["direction"] = otherAction[0:5]
            tmp_other_action["pass"] = otherAction[5:10]
            otherAction = tmp_other_action

        if invert_other_action:
            otherAction["direction"] = self.invert_actions(otherAction["direction"])

        all_directions = np.append(action["direction"], otherAction["direction"])
        all_pass = np.append(action["pass"], otherAction["pass"])
        keys = list(self.state.keys())
        for n in range(len(self.state)):
            self.state[keys[n]] = (all_directions[n], all_pass[n])

        # collect reward if it is the last agent to act

        # update players - actions will occur in random order
        random_order = list(self.state.keys())
        random.shuffle(random_order)
        is_there_score = (False, "")
        for agent_actions in random_order:
            player = self.observations[agent_actions]
            ball = self.observations["ball"]
            deg = self.state[agent_actions][0]
            pass_ball = self.state[agent_actions][1]
            if self.mode == "atari":
                deg = int(deg * 45)

            ### do you have the ball?
            has_ball = bool(player["has_ball"])

            if has_ball:
                ### if you have the ball have you scored?
                if player["team"] == 1:
                    if ball["x"] <= 1:
                        # print("score!")
                        is_there_score = (True, player["team"])
                elif player["team"] == 0:
                    if ball["x"] >= 11:
                        # print("score!")
                        is_there_score = (True, player["team"])

                if deg == 360:
                    # do nothing
                    pass
                elif pass_ball:
                    # pass the ball
                    # ball["velocity_x"] = np.round(BALL_MOVEMENT * math.sin(deg * math.pi / 180),2)
                    # ball["velocity_y"] = np.round(BALL_MOVEMENT * math.cos(deg * math.pi / 180),2)
                    ball["velocity_x"] = BALL_MOVEMENT * math.sin(deg * math.pi / 180)
                    ball["velocity_y"] = BALL_MOVEMENT * math.cos(deg * math.pi / 180)

                else:
                    # moving with the ball
                    # x_movement = np.round(PLAYER_MOVEMENT * math.sin(deg * math.pi / 180),2)
                    # y_movement = np.round(PLAYER_MOVEMENT * math.cos(deg * math.pi / 180),2)
                    x_movement = PLAYER_MOVEMENT * math.sin(deg * math.pi / 180)
                    y_movement = PLAYER_MOVEMENT * math.cos(deg * math.pi / 180)

                    player["x"] = player["x"] + x_movement
                    player["y"] = player["y"] + y_movement
                    #
                    # player['x'] = 1
                    # player['y'] = 0

                    player["x"] = min([max([0, player["x"]]), LENGTH])
                    player["y"] = min([max([0, player["y"]]), WIDTH])
                    ball["x"] = player["x"]
                    ball["y"] = player["y"]
                    ball["velocity_x"] = 0
                    ball["velocity_y"] = 0

            if has_ball == False and deg != 360:
                # x_movement = np.round(PLAYER_MOVEMENT * math.sin(deg * math.pi / 180),2)
                # y_movement = np.round(PLAYER_MOVEMENT * math.cos(deg * math.pi / 180),2)
                x_movement = PLAYER_MOVEMENT * math.sin(deg * math.pi / 180)
                y_movement = PLAYER_MOVEMENT * math.cos(deg * math.pi / 180)

                player["x"] = player["x"] + x_movement
                player["y"] = player["y"] + y_movement
                #
                # player['x'] = 1
                # player['y'] = 0

                player["x"] = min([max([0, player["x"]]), LENGTH])
                player["y"] = min([max([0, player["y"]]), WIDTH])

            elif deg == 360:
                # no movement
                pass

            # only used for academy
            if self.flags != []:
                ### flag checks
                for flag in self.flags:
                    if flag["trigger"] == "player":
                        if abs(player["x"] - flag["x"]) < 0.5:
                            if abs(player["y"] - flag["y"]) < 0.5:
                                flag["colour"] = "green"
                    elif flag["trigger"] == "ball":
                        if abs(ball["x"] - flag["x"]) < 0.5:
                            if abs(ball["y"] - flag["y"]) < 0.5:
                                flag["colour"] = "green"
                    elif flag["trigger"] == "player_and_ball":
                        if abs(ball["x"] - flag["x"]) < 0.5:
                            if abs(ball["y"] - ball["y"]) < 0.5:
                                if abs(player["x"] - flag["x"]) < 0.5:
                                    if abs(player["y"] - flag["y"]) < 0.5:
                                        if player["has_ball"] == 1:
                                            flag["colour"] = "green"
                    else:
                        print("No valid trigger")
                        raise Exception

        # last move the ball
        self.observations["ball"]["x"] += self.observations["ball"]["velocity_x"]
        self.observations["ball"]["y"] += self.observations["ball"]["velocity_y"]

        if self.observations["ball"]["x"] < 0:
            self.observations["ball"]["x"] = 0 - self.observations["ball"]["x"]
            self.observations["ball"]["velocity_x"] = -self.observations["ball"][
                "velocity_x"
            ]
        elif self.observations["ball"]["x"] > LENGTH:
            self.observations["ball"]["x"] = LENGTH - (
                self.observations["ball"]["x"] - LENGTH
            )
            self.observations["ball"]["velocity_x"] = -self.observations["ball"][
                "velocity_x"
            ]
        if self.observations["ball"]["y"] < 0:
            self.observations["ball"]["y"] = 0 - self.observations["ball"]["y"]
            self.observations["ball"]["velocity_y"] = -self.observations["ball"][
                "velocity_y"
            ]
        elif self.observations["ball"]["y"] > WIDTH:
            self.observations["ball"]["y"] = WIDTH - (
                self.observations["ball"]["y"] - WIDTH
            )
            self.observations["ball"]["velocity_y"] = -self.observations["ball"][
                "velocity_y"
            ]

        ### ball has friction
        self.observations["ball"]["velocity_x"] = (
            self.observations["ball"]["velocity_x"]
        ) * 0.99

        self.observations["ball"]["velocity_y"] = (
            self.observations["ball"]["velocity_y"]
        ) * 0.99

        ### who captures the ball? If anyone?
        ### what's the distance to the ball - using Pythagoras
        potential_ball_holders = []
        for agent_actions in random_order:
            player = self.observations[agent_actions]

            # for moment no one has ball
            player["has_ball"] = 0
            ball = self.observations["ball"]

            distance_to_ball = (
                (abs(ball["x"] - player["x"])) ** 2
                + (abs(ball["y"] - player["y"])) ** 2
            ) ** 0.5
            if distance_to_ball < PLAYER_TACKLE_LIMIT:
                potential_ball_holders.append({"agent":agent_actions, "team":player['team'], "distance_to_ball":distance_to_ball})

        if len(potential_ball_holders) == 0:
            pass
        else:
            # who gets ball
            ### get final 2 candidates
            closest_team_0 = {"distance_to_ball":99}
            closest_team_1 = {"distance_to_ball":99}

            for agent in potential_ball_holders:
                if agent['team'] ==0:
                    if agent['distance_to_ball'] < closest_team_0['distance_to_ball']:
                        closest_team_0['distance_to_ball'] = agent['distance_to_ball']
                        closest_team_0['agent'] = agent['agent']
                if agent['team'] ==1:
                    if agent['distance_to_ball'] < closest_team_1['distance_to_ball']:
                        closest_team_1['distance_to_ball'] = agent['distance_to_ball']
                        closest_team_1['agent'] = agent['agent']

            single_agent_potential_ball_holders = []
            for ele in [closest_team_0, closest_team_1]:
                if ele['distance_to_ball']==99:
                    pass
                else:
                    single_agent_potential_ball_holders.append(ele['agent'])


            agent_gets_ball = random.choice(single_agent_potential_ball_holders)
            self.observations[agent_gets_ball]["has_ball"] = 1
            #self.observations[agent_gets_ball]["x"] = self.observations["ball"]["x"]
            #self.observations[agent_gets_ball]["y"] = self.observations["ball"]["y"]
            self.observations["ball"]["x"] = self.observations[agent_gets_ball]["x"]
            self.observations["ball"]["y"] = self.observations[agent_gets_ball]["y"]
            self.observations["ball"]["velocity_x"] = 0
            self.observations["ball"]["velocity_y"] = 0

        # rewards
        if is_there_score[0]:
            ### add score to array
            if is_there_score[1] == 0:
                # red scored
                reward = 1
            else:
                reward = -1

            # once either play wins or there is a draw, game over, both players are done
            terminations = True
            # self.agents = []

        else:
            reward = 0
            terminations = False

        if self.flags != []:
            terminations = True
            for flag in self.flags:
                if flag["colour"] == "red":
                    terminations = False

        if self.render_mode == True:
            self.render()

        observations = self.observe()
        if self.old_api:
            return observations, reward, terminations, self.observations
        else:
            return observations, reward, terminations, {}, self.observations
