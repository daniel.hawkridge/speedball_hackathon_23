import pygame
from speedball.pygame_config import (
    BLACK,
    PYGAME_LENGTH,
    PYGAME_WIDTH,
    divisor,
    LENGTH,
    WIDTH,
    offset,
    offset_multiplier,
    WHITE,
    RED,
    BLUE,
    GOLD,
)
import os

this_dir, this_filename = os.path.split(__file__)

ESSENCE_LOGO = pygame.image.load(os.path.join(this_dir, "img", "em.png"))
RED_MAN = pygame.image.load(os.path.join(this_dir, "img", "red_man.png"))
BLUE_MAN = pygame.image.load(os.path.join(this_dir, "img", "blue_man.png"))
BALL = pygame.image.load(os.path.join(this_dir, "img", "ball.svg"))
RED_FLAG = pygame.image.load(os.path.join(this_dir, "img", "red_flag.png"))
GREEN_FLAG = pygame.image.load(os.path.join(this_dir, "img", "green_flag.png"))


def pygame_get_sprites(agents, ball, teams):
    # add players and ball to sprites group

    all_sprites = pygame.sprite.Group()

    # players
    for element in agents:
        x = (element["x"]) * PYGAME_LENGTH / LENGTH
        y = (element["y"]) * PYGAME_WIDTH / WIDTH
        if element["team"] == teams[0]["team"]:
            player = Red_team((x, y))
        else:
            player = Blue_team((x, y))
        all_sprites.add(player)
    # ball
    x = (ball["x"]) * PYGAME_LENGTH / LENGTH
    y = (ball["y"]) * PYGAME_WIDTH / WIDTH
    ball = Ball((x, y))
    all_sprites.add(ball)
    return all_sprites


def pygame_get_sprites_v2(agents, ball, teams, flags=[]):
    # add players and ball to sprites group

    all_sprites = pygame.sprite.Group()

    # players
    try:
        for name in agents:
            element = agents[name]
            x = offset + (element["x"]) * (PYGAME_LENGTH - (2 * offset)) / LENGTH
            y = offset + ((element["y"]) * (PYGAME_WIDTH - (3.5 * offset)) / (WIDTH))
            if element["team"] == 0:
                player = Red_team((x, y))

            elif element["team"] == 1:
                player = Blue_team((x, y))
            all_sprites.add(player)

    except:
        # ball
        x = offset + (ball["x"]) * (PYGAME_LENGTH - (2 * offset)) / LENGTH
        y = offset + ((ball["y"]) * (PYGAME_WIDTH - (3.5 * offset)) / (WIDTH))
        ball = Ball((x, y))
        all_sprites.add(ball)
        essence = Essence_logo()

        all_sprites.add(essence)

    for flag in flags:
        x = offset + (flag["x"]) * (PYGAME_LENGTH - (2 * offset)) / LENGTH
        y = offset + ((flag["y"]) * (PYGAME_WIDTH - (3.5 * offset)) / (WIDTH))
        if flag["colour"] == "red":
            flag_sprite = RedFlag((x, y))
        elif flag["colour"] == "green":
            flag_sprite = GreenFlag((x, y))
        all_sprites.add(flag_sprite)

    return all_sprites


class Essence_logo(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        DATA_PATH = os.path.join(this_dir, "img", "em.png")
        self.image = pygame.transform.scale(
            ESSENCE_LOGO, (PYGAME_LENGTH * 0.6, PYGAME_WIDTH * 0.08)
        )
        # self.image.set_colorkey(WHITE)
        # self.image.fill(GREEN)
        self.rect = self.image.get_rect()
        self.rect.center = (PYGAME_LENGTH / 2, PYGAME_WIDTH * 0.92)


class Red_team(pygame.sprite.Sprite):
    def __init__(self, loc):
        pygame.sprite.Sprite.__init__(self)
        DATA_PATH = os.path.join(this_dir, "img", "red_man.png")
        self.image = pygame.transform.scale(RED_MAN, (offset / 2, offset / 2))
        # self.image.set_colorkey(WHITE)
        # self.image.fill(GREEN)
        self.rect = self.image.get_rect()
        self.rect.center = (loc[0], loc[1])
        # self.rect.right = loc[0]
        # self.rect.bottom = loc[1]


class Blue_team(pygame.sprite.Sprite):
    def __init__(self, loc):
        pygame.sprite.Sprite.__init__(self)
        DATA_PATH = os.path.join(this_dir, "img", "blue_man.png")
        self.image = pygame.transform.scale(BLUE_MAN, (offset / 2, offset / 2))
        # self.image.set_colorkey(WHITE)
        # self.image.fill(GREEN)
        self.rect = self.image.get_rect()
        self.rect.center = (loc[0], loc[1])


class Ball(pygame.sprite.Sprite):
    def __init__(self, loc):
        pygame.sprite.Sprite.__init__(self)
        DATA_PATH = os.path.join(this_dir, "img", "ball.svg")
        self.image = pygame.transform.scale(BALL, (offset / 4, offset / 4))
        # self.image.set_colorkey(WHITE)
        # self.image.fill(GREEN)
        self.rect = self.image.get_rect()
        # self.rect.right = loc[0]
        # self.rect.bottom = loc[1]
        self.rect.center = (loc[0], loc[1])


class RedFlag(pygame.sprite.Sprite):
    def __init__(self, loc):
        pygame.sprite.Sprite.__init__(self)
        DATA_PATH = os.path.join(this_dir, "img", "red_flag.png")
        self.image = pygame.transform.scale(RED_FLAG, (offset / 4, offset / 4))
        # self.image.set_colorkey(WHITE)
        # self.image.fill(GREEN)
        self.rect = self.image.get_rect()
        # self.rect.right = loc[0]
        # self.rect.bottom = loc[1]
        self.rect.center = (loc[0], loc[1])


class GreenFlag(pygame.sprite.Sprite):
    def __init__(self, loc):
        pygame.sprite.Sprite.__init__(self)
        DATA_PATH = os.path.join(this_dir, "img", "green_flag.png")
        self.image = pygame.transform.scale(GREEN_FLAG, (offset / 4, offset / 4))
        # self.image.set_colorkey(WHITE)
        # self.image.fill(GREEN)
        self.rect = self.image.get_rect()
        # self.rect.right = loc[0]
        # self.rect.bottom = loc[1]
        self.rect.center = (loc[0], loc[1])


# font_name = pygame.font.match_font('freeserif')
# pygame.font.init()


def render_text(surf, text, size, x, y, font_name, colour):
    font = pygame.font.Font(font_name, size)
    text_surface = font.render(text, True, colour)
    text_rect = text_surface.get_rect()
    text_rect.midtop = (x, y)
    surf.blit(text_surface, text_rect)


def draw_score(surf, score, size, x, y):
    DATA_PATH = os.path.join(this_dir, "img", "retro_font.ttf")
    font_name = DATA_PATH
    colour = RED
    for team in score:
        text = "{}: {}".format(team, score[team])
        render_text(surf, text, size, x, y, font_name, colour)
        y += 60
        colour = BLUE


def draw_text(surf, text, size, x, y, colour):
    DATA_PATH = os.path.join(this_dir, "img", "retro_font.ttf")
    font_name = DATA_PATH
    render_text(surf, text, size, x, y, font_name, colour)
