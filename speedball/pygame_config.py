import os

divisor = 2
PYGAME_LENGTH = int(2800 / divisor)
PYGAME_WIDTH = int(1900 / divisor)
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
BLUE = (0, 127, 255)
RED = (255, 0, 0)
GOLD = (207, 181, 59)
offset = 200 / divisor
offset_multiplier = 1 + (offset / PYGAME_LENGTH)
LENGTH = 12
WIDTH = 7
PLAYER_MOVEMENT = 0.2
BALL_MOVEMENT = 0.4
# player tackle is
PLAYER_TACKLE_SD = PLAYER_MOVEMENT * 0.07 / 0.01
PLAYER_TACKLE_LIMIT = PLAYER_MOVEMENT * 1.9

this_dir, this_filename = os.path.split(__file__)
FIELD = os.path.join(this_dir, "img", "field5.png")
