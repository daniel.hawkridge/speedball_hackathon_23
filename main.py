from speedball import speedball_gym
import random


def your_agent(obs, info):
    """
    A template for your agent
    """
    direction = [random.randint(0, 360) for n in range(5)]
    passing = [0 for n in range(5)]
    return {"direction": direction, "pass": passing}

env = speedball_gym.SpeedballEnv(observations="array", render_mode=True, fps=15)
obs, info = env.reset()

score_store = {"red_team":0, "blue_team":0}
for n in range(10000):
    actions = your_agent(obs, info)
    other_actions = env.action_space.sample()
    obs, reward, done, _, info = env.step(actions, otherAction=other_actions)
    if done:
        if reward==1:
            score_store['red_team']+=1
        else:
            score_store['blue_team']+=1
        print(score_store)
        env.reset()

env.close()