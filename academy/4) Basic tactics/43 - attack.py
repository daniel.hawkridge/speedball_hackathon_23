"""
Put what you've learned together, and try score in a 3 on 2 scenario vs the blue team.

No flags anymore - we're now using the actual Speedball scoring mechanism - be in possesion of the ball in
your opponents endzone!

Notice how they are defending - can you take advantage of that?

"""

from speedball import speedball_gym
import random


env = speedball_gym.SpeedballEnv(
    render_mode=True, players_per_team=3, number_of_agents=5, ball_starts_with="red"
)


obs, info = env.reset()
for n in range(200):
    ### Your code lives here

    actions = {"direction": [360, 360, 360], "pass": [0, 0, 0]}

    ### ends here

    ### the other agent - don't change this!
    if info["1_0"]["y"] <= info["ball"]["y"]:
        otheractions = {"direction": [0, 0], "pass": [0, 0]}
    else:
        otheractions = {"direction": [180, 180], "pass": [0, 0]}
    if info["1_0"]["has_ball"] or info["1_1"]["has_ball"]:
        otheractions = {"direction": [270, 270], "pass": [1, 1]}
    ### other agent ends

    obs, reward, done, _, info = env.step(actions, otherAction=otheractions)
    if done:
        print("You passed!")
        break

if not done:
    print("Try again!")

env.close()
