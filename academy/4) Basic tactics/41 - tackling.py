"""
Let's introduce the blue team. Take the ball off them and throw to the flag.

Ball possesion / tackling is automatic - you need to be within 0.4 units of the ball. If more than 1 agent is within
this threshold, possesion will be decided via a random choice.

IMPORTANT TACTICS
Agent movement is 0.2 units per step
Agent passing is 0.4 units per step

If you gain possesion it's likely passing is a better option than running with the ball
If you're defending be aware passes are much faster than throwing - do you narrow the angle the opposition can throw in?
Or hold a zonal defence?
"""

from speedball import speedball_gym


flags = [{"colour": "red", "x": 11.5, "y": 6, "trigger": "ball"}]
env = speedball_gym.SpeedballEnv(
    render_mode=True,
    flags=flags,
    players_per_team=1,
    number_of_agents=2,
    ball_starts_with="blue",
)


obs, info = env.reset()
for n in range(80):
    ### Your code lives here
    actions = {"direction": [360], "pass": [0]}
    ### ends here

    otheractions = {"direction": [360], "pass": [0]}

    obs, reward, done, _, info = env.step(actions, otherAction=otheractions)
    if done:
        print("You passed!")
        break

if not done:
    print("Try again!")

env.close()
