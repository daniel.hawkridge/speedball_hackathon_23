"""
The agent will move randomly now - try to tackle and throw to flag again

"""

from speedball import speedball_gym
import random


flags = [{"colour": "red", "x": 11.5, "y": 6, "trigger": "ball"}]
env = speedball_gym.SpeedballEnv(
    render_mode=True,
    flags=flags,
    players_per_team=1,
    number_of_agents=2,
    ball_starts_with="blue",
)


obs, info = env.reset()
for n in range(80):
    ### Your code lives here
    actions = {"direction": [360], "pass": [0]}
    ### ends here

    otheractions = {"direction": [random.randint(0, 360)], "pass": [0]}

    obs, reward, done, _, info = env.step(actions, otherAction=otheractions)
    if done:
        print("You passed!")
        break

if not done:
    print("Try again!")

env.close()
