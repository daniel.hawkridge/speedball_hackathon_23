"""
How do you move agents?

Agents are controlled via actions. You control all your agents at once, at the same time.

Actions are constructed as a dictionary:

{
"direction":[angle, angle, ...],
"pass":[pass, pass, ...]
}

where angle is in the range 0-360 (360 keeping the agent still), and pass a boolean (0 or 1)

Random actions can be aquired by calling:
env.action_space.sample()

See code for example:
"""

from speedball import speedball_gym

env = speedball_gym.SpeedballEnv(render_mode=True)

obs, info = env.reset()
for n in range(100):
    actions = {"direction": [0, 90, 180, 270, 360], "pass": [0, 0, 1, 0, 0]}


    obs, reward, done, _, info = env.step(actions, otherAction=actions)
    if done:
        print(reward)
        env.reset()

env.close()
