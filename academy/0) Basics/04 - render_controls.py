"""
Rendering is great to see what's going on with your agents. Use the fps argument to control how fast or slow you want
to render.

Note rendering has a limit of speed - you can't keep pushing up the fps, at a certain point the code can't render fast enough.

If you want to test what you've built - don't render!

"""

from speedball import speedball_gym
from datetime import datetime

env = speedball_gym.SpeedballEnv(observations="array", render_mode=True, fps=1e6)
obs, info = env.reset()

start = datetime.now()
for n in range(1000):
    actions = env.action_space.sample()
    obs, reward, done, _, info = env.step(actions)
    if done:
        print(reward)
        env.reset()
end = datetime.now()

with_rendering = end - start


env = speedball_gym.SpeedballEnv(observations="array", render_mode=False, fps=1e6)
obs, info = env.reset()

start = datetime.now()
for n in range(1000):
    actions = env.action_space.sample()
    obs, reward, done, _, info = env.step(actions)
    if done:
        print(reward)
        env.reset()
end = datetime.now()
without_rendering = end - start

print(
    f"rendering 1000 steps took {with_rendering}, without rendering took {without_rendering}, a speed increase of {with_rendering / without_rendering}"
)

env.close()