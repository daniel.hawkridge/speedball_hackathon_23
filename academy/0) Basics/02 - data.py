"""
Let's talk data.

On calling reset, you get 2 data points:
info - a human readable nested dictionary, describing the xy co-ordinates of agents, if they have the ball, and
the team they are on, and the ball xy co-ordinates and xy velocity
obs - the same data as human, but flattened into a 1D array

When calling step you get an additional 3 points of data:
reward - 0 if not done, 1 if team 0 (red team) scores  or -1 if team 1 (blue team) scores.
done - someone has scored
truncations - not used in speedball but needed for AIGym compatibility, set as _ throughout codebase

Run the script below - it'll run for one step and print out each data point

"""


from speedball import speedball_gym

env = speedball_gym.SpeedballEnv(render_mode=True, fps=30)

obs, info = env.reset()
print("human observations")
print(info)
print("")
print("array observations")
print(obs)


actions = env.action_space.sample()
obs, reward, done, _, info = env.step(actions)

print("reward: ", reward)
print("done: ", done)

env.close()
