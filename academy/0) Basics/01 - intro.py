"""
Welcome to the Speedball academy!

Speedball is a competitive agent vs agent environment, where the goal is to be in posession of the ball in
your oppenents endzone. It uses the OpenAI Gym API (v26) to control the environment.

You can pass and move with the ball, and tackle your opponents when they have the ball.

Run the file to see it in action.

"""


from speedball import speedball_gym

# the env is the core object in speedball - it records game state, mechanics and updates
env = speedball_gym.SpeedballEnv(render_mode=True, fps=15)

# reset sets the environment up for a new game
obs, info = env.reset()

# each game is seperated into steps - a step is where every agent and the ball moves ones
for n in range(100000):
    # this randomly samples an available move
    actions = env.action_space.sample()
    # actions are passed to the step method, which updates the game state
    obs, reward, done, _, info = env.step(actions)
    # if done - there's a score - print the result, reset the environment
    if done:
        print(reward)
        env.reset()


env.close()