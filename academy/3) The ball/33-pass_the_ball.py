"""
Your objective is now to pick up, and pass the ball to the flag - use the pass argument in actions (look at 03 - actions.py)

"""

from speedball import speedball_gym


flags = [{"colour": "red", "x": 11.5, "y": 1, "trigger": "ball"}]
env = speedball_gym.SpeedballEnv(
    render_mode=True, flags=flags, players_per_team=1, number_of_agents=1
)


obs, info = env.reset()
for n in range(55):
    ### Your code lives here
    if info["0_0"]["has_ball"] == 0:
        actions = {"direction": [0], "pass": [0]}
    else:
        actions = {"direction": [0], "pass": [1]}
    ### ends here

    obs, reward, done, _, info = env.step(actions)
    if done:
        print("You passed!")
        break

if not done:
    print("Try again!")

env.close()
