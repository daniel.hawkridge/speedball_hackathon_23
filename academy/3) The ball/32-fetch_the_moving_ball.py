"""
Same objective as before, but the ball moves too - each time with a different random velocity.
Your agent will need to follow the ball, and need some foresight to see where the ball will be in the future.

This might take a while - but it's a key part of your agents behaviour when you are playing the full game!

"""

from speedball import speedball_gym


flags = [{"colour": "red", "x": 11.5, "y": 1, "trigger": "player_and_ball"}]
env = speedball_gym.SpeedballEnv(
    render_mode=True,
    flags=flags,
    players_per_team=1,
    number_of_agents=1,
    random_ball_velocity=True,
)


obs, info = env.reset()
for n in range(80):
    ### Your code lives here
    actions = {"direction": [0], "pass": [0]}
    ### ends here

    obs, reward, done, _, info = env.step(actions)
    if done:
        print("You passed!")
        break

if not done:
    print("Try again!")

env.close()