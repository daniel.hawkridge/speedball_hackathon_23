"""
Let's introduce the ball. Fetch it and move it to the flag.

To pick up the ball - run at it. You'll automatically pick it up when you're close enough.

"""

from speedball import speedball_gym


flags = [{"colour": "red", "x": 11.5, "y": 1, "trigger": "player_and_ball"}]
env = speedball_gym.SpeedballEnv(
    render_mode=True, flags=flags, players_per_team=1, number_of_agents=1
)


obs, info = env.reset()
for n in range(80):
    ### Your code lives here
    if info["0_0"]["has_ball"] == 0:
        actions = {"direction": [0], "pass": [0]}
    else:
        actions = {"direction": [0], "pass": [0]}
    ### ends here

    obs, reward, done, _, info = env.step(actions)
    if done:
        print("You passed!")
        break

if not done:
    print("Try again!")

env.close()
