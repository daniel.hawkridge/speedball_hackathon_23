"""
Welcome to the Speedball academy!

Here you'll learn how to control your agents, play the game, and basic tactics and strategy.

First things first, movement. Can you create code that moves an agent from its starting position to the red flag?

Ignore the ball for the moment

You have 100 steps to complete this challenge

"""

from speedball import speedball_gym


flags = [{"colour": "red", "x": 11.5, "y": 3, "trigger": "player"}]
env = speedball_gym.SpeedballEnv(
  flags=flags, players_per_team=1, number_of_agents=1
)


obs, info = env.reset()
for n in range(100):
    ### Your code lives here
    actions = {"direction": [360], "pass": [0]}
    ### ends here

    obs, reward, done, _, info = env.step(actions)
    if done:
        print("You passed!")
        break

if not done:
    print("Try again!")

env.close()
