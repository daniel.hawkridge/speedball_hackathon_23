"""
The second challenge is multiple flags.

You'll need to use a cleverer agent here - try using the observations to decide where to go next

Again ignore the ball. Your challenge is to visit each flag in any order.

"""

from speedball import speedball_gym


flags = [
    {"colour": "red", "x": 11.5, "y": 0, "trigger": "player"},
    {"colour": "red", "x": 5, "y": 5, "trigger": "player"},
    {"colour": "red", "x": 2, "y": 1, "trigger": "player"},
    {"colour": "red", "x": 9, "y": 3, "trigger": "player"},
]
env = speedball_gym.SpeedballEnv(
    render_mode=True, flags=flags, players_per_team=1, number_of_agents=1
)


obs, info = env.reset()
for n in range(100):
    ### Your code lives here
    actions = {"direction": [360], "pass": [0]}
    ### ends here

    obs, reward, done, _, info = env.step(actions)
    if done:
        print("You passed!")
        break

if not done:
    print("Try again!")

env.close()
