"""
In a rush to validate an idea? Use the multiprocessing library.

This enables you to run multiple speedball games at the same time by using multiple cores of the machine. This isn't
guaranteed to be faster in all circumstances, but may prove helpful.


"""
from speedball import speedball_gym
import random
import multiprocessing


def random_agent(obs, info):
    """
    An agent that returns a random action
    """
    direction = [random.randint(0, 360) for n in range(5)]
    passing = [0 for n in range(5)]
    return {"direction": direction, "pass": passing}


def thread_function(name):
    reward_store = []
    env = speedball_gym.SpeedballEnv(render_mode=False)


    obs, info = env.reset()
    for n in range(10000):
        actions = random_agent(obs, info)
        otheractions = random_agent(obs, info)
        obs, reward, done, _, info = env.step(actions, otherAction=otheractions)
        if done:
            reward_store.append(reward)
            obs, info = env.reset()
    env.close()
    return reward_store

processes = 6
tasks = processes
pool = multiprocessing.Pool(processes=processes)
result = pool.map(thread_function,[n for n in range(tasks)])
all_results = []
for ele in result:
    all_results = all_results + ele
print(all_results)


