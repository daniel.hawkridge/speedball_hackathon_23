"""
Tip 2 - the inverters

The inverter allows you to see every game as if you were the red team. It flips the observations, and actions, such that
you only need to code 1 agent (red vs blue), not 2 (red vs blue, blue vs red).

This is not required if you don't want to use it - but you will need to code both playing as red and blue otherwise.

The inverter comes in 2 functions:

env.inverter(obs,info) - inverts the obs and info, takes obs and info as an input
env.invert_actions(directions) - inverts the directions of actions

This is a really powerful tool - rather than worrying if you're playing better as red or blue, you'll be able to
just write one agent playing as red - feed it into these functions, and get a blue agent that plays the same.

Examples below - showing via assert statements that inverted observations and actions are working as expected - actions
and observations are inverted.


"""

from speedball import speedball_gym
import random
import numpy as np


def random_agent(obs, info):
    """
    An agent that returns a random action
    """
    direction = [random.randint(0, 360) for n in range(5)]
    passing = [0 for n in range(5)]
    return {"direction": direction, "pass": passing}


env = speedball_gym.SpeedballEnv(render_mode=True, fps=30)


obs, info = env.reset()
inv_obs, inv_info = env.inverter(obs, info)
assert np.array_equal(obs, inv_obs)
assert info == inv_info


for n in range(50):
    actions = {"direction": [90, 90, 90, 90, 90], "pass": [0, 0, 0, 0, 0]}
    otheractions = {"direction": [270, 270, 270, 270, 270], "pass": [0, 0, 0, 0, 0]}
    obs, reward, done, _, info = env.step(actions, otherAction=otheractions)

    inv_obs, inv_info = env.inverter(obs, info)
    assert np.array_equal(obs, inv_obs)
    assert info == inv_info
    if done:
        break


env = speedball_gym.SpeedballEnv(render_mode=True, fps=30)


obs, info = env.reset()
inv_obs, inv_info = env.inverter(obs, info)
assert np.array_equal(obs, inv_obs)
assert info == inv_info


for n in range(30):
    actions = random_agent(obs, info)
    otheractions = {
        "direction": env.invert_actions(actions["direction"]),
        "pass": [0, 0, 0, 0, 0],
    }

    obs, reward, done, _, info = env.step(actions, otherAction=otheractions)

    inv_obs, inv_info = env.inverter(obs, info)
    if np.array_equal(obs, inv_obs) == False:
        print(obs == inv_obs)
        env.inverter(obs, info)
    assert info == inv_info
    if done:
        break

env.close()