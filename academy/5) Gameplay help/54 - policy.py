"""
Policies are an advanced concept used for deep learning libraries - if you're not going to do this don't worry about
this section!

Policies allow you to embed a function into the environment before running it for the blue team.

Rather than generating otherAction on every step of play it automatically feeds observations into the function and gets
 actions for the blue team. It automatically feeds the policy with inverted data, and inverts the outputs.

WHY:

Speedball is built on the AI Gym framework which is a Player vs Environment (PvE) set up - you get an observation, and
take an action. But we have 2 separate policies, one for the red team, and one for the blue team. Most AI frameworks
for training using the AIGym library will not allow otherAction= to be passed, so how do you train a new agent vs a
pre-trained existing agent using these libraries?

Policies allow us to ignore this - the algorithms just see they are playing against the environment

Because of the inverter automatically applying you can use a trained model output as the policy for a new round of training.

You've already used policies in earlier examples - anytime you don't put an otherAction argument in you use the default
random action policy:

def policy(env, observations, info):
    return env.action_space.sample()

Example below - complex concept but very easy to implement:
"""

from speedball import speedball_gym
import random
import numpy as np


def policy_agent(obs, info):
    """
    An agent that returns a random action
    """
    direction = [90 for n in range(5)]
    passing = [0 for n in range(5)]
    return {"direction": direction, "pass": passing}


env = speedball_gym.SpeedballEnv(render_mode=True, policy=policy_agent)


obs, info = env.reset()

for n in range(50):
    actions = {"direction": [90, 90, 90, 90, 90], "pass": [0, 0, 0, 0, 0]}
    obs, reward, done, _, info = env.step(actions)
    if done:
        break

env.close()
