"""
In a competitive game you'll play against other teams agents both as the red team, and the blue team.

Your agents need to be able to play in both directions - all academy challenges have you playing as the red team so far

You play as the blue team by passing actions to otherAction in .step() - we did this for 43, where the academy agent
played as the defensive role, or in 51 where each function was it's own agent

"""

from speedball import speedball_gym
import random


def random_agent(obs, info):
    """
    An agent that returns a random action
    """
    direction = [random.randint(0, 360) for n in range(5)]
    passing = [0 for n in range(5)]
    return {"direction": direction, "pass": passing}


def defense_agent(obs, info):
    """
    An agent that defends by moving to match the y of the ball, and passes away if has ball
    """
    if info["1_0"]["y"] <= info["ball"]["y"]:
        action = {"direction": [0, 0, 0, 0, 0], "pass": [0, 0, 0, 0, 0]}
    else:
        action = {"direction": [180, 180, 180, 180, 180], "pass": [0, 0, 0, 0, 0]}
    if info["1_0"]["has_ball"] or info["1_1"]["has_ball"]:
        action = {"direction": [270, 270, 270, 270, 270], "pass": [1, 1, 1, 1, 1]}
    return action


env = speedball_gym.SpeedballEnv(render_mode=True, ball_starts_with="red")


obs, info = env.reset()
for n in range(200):
    actions = random_agent(obs, info)
    otheractions = defense_agent(obs, info)
    obs, reward, done, _, info = env.step(actions, otherAction=otheractions)
    if done:
        break

env.close()
