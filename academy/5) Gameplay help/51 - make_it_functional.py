"""
This module will help you navigate building an agent - you don't need to write any code, just run the scenarios.

Tip 1 - make it a function

Code living in a for loop is going to get confusing. Write functions for each version of agent you use, with
easy to read names. For example, let's turn the defensive agent from 43 and a random agent into a function:

"""

from speedball import speedball_gym
import random


def random_agent(obs, info):
    """
    An agent that returns a random action
    """
    direction = [random.randint(0, 360) for n in range(5)]
    passing = [0 for n in range(5)]
    return {"direction": direction, "pass": passing}


def defense_agent(obs, info):
    """
    An agent that defends by moving to match the y of the ball, and passes away if has ball
    """
    if info["1_0"]["y"] <= info["ball"]["y"]:
        action = {"direction": [0, 0, 0, 0, 0], "pass": [0, 0, 0, 0, 0]}
    else:
        action = {"direction": [180, 180, 180, 180, 180], "pass": [0, 0, 0, 0, 0]}
    if info["1_0"]["has_ball"] or info["1_1"]["has_ball"]:
        action = {"direction": [270, 270, 270, 270, 270], "pass": [1, 1, 1, 1, 1]}
    return action


env = speedball_gym.SpeedballEnv(render_mode=True, ball_starts_with="red")


obs, info = env.reset()
for n in range(200):
    actions = random_agent(obs, info)
    otheractions = defense_agent(obs, info)
    obs, reward, done, _, info = env.step(actions, otherAction=otheractions)
    if done:
        break

env.close()