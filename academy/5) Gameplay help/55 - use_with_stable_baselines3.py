"""

Stable baselines3 is a Reinforcement learning package used with AIGym. It will allow you to automatically train an
agent to play Speedball.

This is added as an example of how to communicate with these packages, rather than an intended starting point for a
solution. Speedball is high dimensional problem and is not well suited to RL solutions with its current feature set, or
reward definition.



DESCRIPTION:

In order to be compatible with commonly used Reinforcement learning packages, the environment has
2 arguments to help

flat_action (default false) - allows a length 10 array to be submitted as an action rather than a dictionary
would look like
multi_discrete.MultiDiscrete([361, 361, 361, 361, 361, 2, 2, 2, 2, 2])
[0_0 direction, 0_1 direction,  0_2 direction,  0_3 direction,  0_4 direction, 0_0 pass, 0_1 pass,  0_2 pass,  0_3 pass,  0_4 pass]

this is needed because most RL packages can't cope with dict action spaces

old_api (default false) - turns the module into an OpenAI v21 interface (https://gymnasium.farama.org/content/migration-guide/)
2 changes - the reset() and step() function responses change.

Let's see this together using stable_baselines3:

"""


from stable_baselines3 import A2C
from speedball import speedball_gym

env = speedball_gym.SpeedballEnv(flat_action=True, old_api=True, render_mode=False)

model = A2C("MlpPolicy", env, verbose=1, tensorboard_log="log/a2c")
# model.load("speedball")
model.learn(total_timesteps=10_000)
reward = 0
for n in model.ep_info_buffer:
    reward += n["r"]
reward = reward / len(model.ep_info_buffer)
print(reward)
print("saving")
model.save("speedball")




### Train a model, then use this to inject it into the environment
global internal_model
internal_model = A2C.load("speedball.zip")

def policy(obs, info):
    actions = internal_model.predict(obs, deterministic=True)[0]
    return actions
env = speedball_gym.SpeedballEnv(flat_action=True, old_api=True, render_mode=True, policy = policy)

obs= env.reset()
for n in range(10000):
    actions = env.action_space.sample()
    obs, reward, done, info = env.step(actions)
    if done:
        print(reward)
        env.reset()

env.close()

