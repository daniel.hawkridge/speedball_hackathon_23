"""
Another level of challenge - can you control multiple agents at the same time?

Ignore the ball - your challenge is to, as a team, visit each flag once.
"""

from speedball import speedball_gym


flags = [
    {"colour": "red", "x": 11.5, "y": 1, "trigger": "player"},
    {"colour": "red", "x": 11.5, "y": 3, "trigger": "player"},
    {"colour": "red", "x": 11.5, "y": 5, "trigger": "player"},
]
env = speedball_gym.SpeedballEnv(
    render_mode=True, flags=flags, players_per_team=3, number_of_agents=3
)


obs, info = env.reset()
for n in range(80):
    ### Your code lives here
    actions = {"direction": [0, 0, 0], "pass": [0, 0, 0]}
    ### ends here

    obs, reward, done, _, info = env.step(actions)
    if done:
        print("You passed!")
        break

if not done:
    print("Try again!")

env.close()
