# Overview:

This is a **python 3.9** AIGym compatible environment designed to demonstrate the use of bots in order to play
a multiplayer (agent) game, called *speedball*. Each team has 5 players (agents) - their objective is to be in possesion of the
ball in the opponents endzone, whilst stopping the opponent from doing the same. For more in-depth overview of the *speedball* game, 
please see the following [deck](https://insidemedia-my.sharepoint.com/:p:/g/personal/daniel_hawkridge_essencemediacom_com/ERjuVHeLnSVHrV924MrP2XoBkbdj5A1V3rf7Tqt-6a3fig?e=8nl9Ip).

# Installation:

We recommend you install [Anaconda](https://www.anaconda.com/) to simplify the 
creation of environments. Download this repo, then in a command line run: 
```commandline
conda create --name speedball python=3.9
conda activate speedball
conda install swig
conda install jupyter
conda install spyder
conda install pytorch==1.13.1 torchvision==0.14.1 pytorch-cuda=11.7 -c pytorch -c nvidia
pip install -r requirements.txt
pip install -e .
```

The `Training` folder contains a set of jupyter notebooks to get familiar with
the overall concepts of agent modelling. The `Academy` folder contains Python 
scripts taking you through the basics of **Speedball**. 


# Challenge, guidance and how to submit:

The goal of the hackathon challenge is to produce an agent that plays speedball. The winning team
is the one that gets the highest score difference after having played every other agent. 

To start the day you need to have a good understanding of how the speedball environment works. Work 
through the academy, getting up to speed with how to interact with agents and the environment mechanics. 

Once you're comfortable with the academy tasks you can move onto the main.py file where there's
a template for you to start your agent.

We strongly recommend you focus on continuous improvement, rather than an idea that could take 
many hours to implement. Something like:

* Design agent - whiteboard, pen and paper
* Code design - built the agent in Python
* Test/validate design (unrendered)  - play the agent vs the previous design
* Play match (rendered) - play the agent vs itself, or previous design
* Update design - use what you've seen to make it better


### Submission

All code for your agent must be contained in a single python file, which is the 
only file you're allowed to submit. You can use any package you see fit to
complete the task, but please indicate what you've used so it can be run
by the organisers. 

You have until 5.40PM to submit an agent, and you may submit as many times as you'd like.
