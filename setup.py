from setuptools import setup, find_packages

setup(name="speedball", packages=find_packages(), include_package_data=True)
